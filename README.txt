
Track User Change module.
------------------------

This module is used to store user last update time. By default during module
installation a new user field "field_user_changed" will be created and 
this field captures user last update time.  
Also this field can be viewed in peoples page.

INSTALL
-------

Just download, unzip/untar and activate the module.
